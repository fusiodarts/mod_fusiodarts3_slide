<?php
/*------------------------------------------------------------------------
# fusiodarts_blank.php
# ------------------------------------------------------------------------
# version		3.0.0
# author    	Angel Albiach - Fusió d'Arts Technology S.L.
# copyright 	Copyright (c) 2016 Fusió d'Arts All rights reserved.
# @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website		http://www.fusiodarts.com
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base(true) . '/media/mod_fusiodarts3_slide/media/mod_fusiodarts3_slide.css');
$document->addScript(JURI::base(true) . '/media/mod_fusiodarts3_slide/media/mod_fusiodarts3_slide.js');

require JModuleHelper::getLayoutPath('mod_fusiodarts3_slide', $params->get('layout', 'default'));
