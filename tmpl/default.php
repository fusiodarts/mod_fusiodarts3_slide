<?php
defined('_JEXEC') or die;
//var_dump($params);

if ($params->get("config_slide_img") == '0') { ?>

<div id="fusioCarousel-<?php echo $module->id; ?>" class="carousel slide <?php echo $params->get('css_main'); ?>" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php
        $i = 0;
        while ($i <= 4) {
            if ($i == 0) {
                $i = 1;
                $class = 'active';
            } else {
                $class = '';
            }
            if ($params->get('active' . $i)) {
                ?>
                <li data-target="#fusioCarousel-<?php echo $module->id; ?>" data-slide-to="<?php echo $i - 1; ?>" class="<?php echo $class; ?>"></li>
            <?php
            }
            $i++;
        }
        ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php
        $i = 1;
        while ($i <= 5) {
            $animate = ($params->get('animate' . $i) == 'd' ? 'fadeInRight' : ($params->get('animate' . $i) == 'c' ? '' : ($params->get('animate' . $i) == 'i' ? 'fadeInLeft' : '')));
            if ($params->get('active' . $i)) {
                require JModuleHelper::getLayoutPath('mod_fusiodarts3_slide', 'default_item');
            }
            $i++;
        }
        ?>
    </div>
    <a class="left carousel-control" href="#fusioCarousel-<?php echo $module->id; ?>" role="button" data-slide="prev">
        <span class="fa fa-arrow-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#fusioCarousel-<?php echo $module->id; ?>" role="button" data-slide="next">
        <span class="fa fa-arrow-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<?php } else { ?>

<div class="fusioImage">
    <?php require JModuleHelper::getLayoutPath('mod_fusiodarts3_slide', 'default_item'); ?>
</div>

<?php } ?>