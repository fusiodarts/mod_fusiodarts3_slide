<?php
defined('_JEXEC') or die;

if ($params->get("config_slide_img") == '0') {
    ($i == 1) ? $active = ' active' : $active = '';

    ($params->get('animation_text' . $i)) ? $a_text = ' animated ' . $params->get('animation_text' . $i) : $a_text = '';
    ($params->get('animation_image' . $i)) ? $a_image = ' animated ' . $params->get('animation_image' . $i) : $a_image = '';

    $text = '';
    if ($params->get('animate' . $i) == 'c') {
        $text = ' col-md-12 t-center';
    } else if ($params->get('animate' . $i) == 'i') {
        $text = ' col-md-6 t-left' . $a_text;
        $image = ' col-md-6' . $a_image;
    } else if ($params->get('animate' . $i) == 'd') {
        $text = ' col-md-6 t-right pull-right' . $a_text;
        $image = ' col-md-6 pull-left' . $a_image;
    }

    if ($params->get("config_title") == '1') {
        // Get default menu - JMenu object, look at JMenu api docs
        $menu = JFactory::getApplication()->getMenu();

        // Get active menu - array with menu items
        $items = $menu->getActive();
        $item = json_decode($items->params);

        $title = $item->page_heading;
        $subtitle = $item->page_subheading;
    } else if ($params->get("config_title") == '0') {
        $title = $params->get('titulo' . $i);
        $subtitle = $params->get('subtitulo' . $i);
    }
    ?>

    <div class="item <?php echo $active; ?> <?php echo $params->get('css' . $i); ?>">
        <img class="first-slide" src="<?php echo $params->get('image' . $i); ?>" alt="First slide">
        <div class="carousel-caption">
            <div class="row">
                <div class="<?php echo $text; ?>">
                    <?php if ($params->get('suptitulo' . $i) != '') { ?>
                        <p><?php echo $params->get('suptitulo' . $i); ?></p>
                    <?php } ?>

                    <?php if ($params->get('titulo' . $i) != '' || $title) { ?>
                        <<?php echo $params->get('header_tag_slide' . $i); ?>><?php echo $title; ?></<?php echo $params->get('header_tag_slide' . $i); ?>>
                    <?php } ?>

                    <?php if ($params->get('subtitulo' . $i) != '' || $subtitle) { ?>
                        <<?php echo $params->get('subheader_tag_slide' . $i); ?>><?php echo $subtitle; ?></<?php echo $params->get('subheader_tag_slide' . $i); ?>>
                    <?php } ?>

                    <?php if ($params->get('boton' . $i) != '') { ?>
                        <p><a class="btn btn-lg btn-primary" href="<?php echo $params->get('link' . $i); ?>" role="button"><?php echo $params->get('boton' . $i); ?></a></p>
                    <?php } ?>
                </div>
                <?php if ($params->get('img-content' . $i) != '') { ?>
                    <div class="img-content <?php echo $image; ?> ">
                        <img class="img-c" src="<?php echo $params->get('img-content' . $i); ?>" alt="Imagen en el contenido">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
} else {
    $i = 1;

    $text = '';
    if ($params->get('animate' . $i) == 'c') {
        $text = 'text-center';
    } else if ($params->get('animate' . $i) == 'i') {
        $text = 'text-left';
    } else if ($params->get('animate' . $i) == 'd') {
        $text = 'text-right';
    }

    if ($params->get("config_title") == '1') {
        // Get default menu - JMenu object, look at JMenu api docs
        $menu = JFactory::getApplication()->getMenu();

        // Get active menu - array with menu items
        $items = $menu->getActive();
        $item = json_decode($items->params);

        $title = $item->page_heading;
        $subtitle = $item->page_subheading;
    } else if ($params->get("config_title") == '0') {
        $title = $params->get('titulo' . $i);
        $subtitle = $params->get('subtitulo' . $i);
    }
    ?>

    <div class="<?php echo $params->get('css' . $i); ?>" style="background: <?php echo $params->get('backgroundcolor_img'); ?> url('<?php echo $params->get('image' . $i); ?>') repeat;">
        <div class="container">
            <div class="row">
                <div class="<?php echo $text; ?>">
                    <?php if ($params->get('titulo' . $i) != '' || $title) { ?>
                        <<?php echo $params->get('header_tag_slide' . $i); ?>><?php echo $title; ?></<?php echo $params->get('header_tag_slide' . $i); ?>>
                    <?php } ?>

                    <?php if ($params->get('subtitulo' . $i) != '' || $subtitle) { ?>
                        <<?php echo $params->get('subheader_tag_slide' . $i); ?>><?php echo $subtitle; ?></<?php echo $params->get('subheader_tag_slide' . $i); ?>>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>